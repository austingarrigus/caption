name="$1"
texttop=$2
textbottom=$3
outname="$4"
width=$(identify -format %w\  "$name" | awk '{print $1;}')
height=$(identify -format %h\  "$name" | awk '{print $1;}')
point=$(echo "($width + $height) / 45 + 10" | bc)
stroke=$(echo "$point / 8" | bc)
distance=$(echo "$stroke + 5" | bc)

convert "$name" \
	-pointsize $point -gravity north -font Anton-Regular -fill white \
	-stroke black -strokewidth $stroke -draw "text 0,$distance \"$texttop\"" \
	-stroke none			   -draw "text 0,$distance \"$texttop\"" \
	-gravity south -stroke black 	   -draw "text 0,$distance \"$textbottom\"" \
	-stroke none 			   -draw "text 0,$distance \"$textbottom\"" \
	"$outname"
