name="$1"
text=$2
outname="$3"
width=$(identify -format %w\  $name | awk '{print $1}')
height=$(identify -format %h\  $name | awk '{print $1}')
point=$(echo "($width + $height) / 30" | bc)
convert "$name" -gravity South -background white -size $width -pointsize $point caption:"$text" +swap -gravity Center -append "$outname"
