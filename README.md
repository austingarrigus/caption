# caption

Adds a caption to an image, meme style.

## Dependencies

Anton-Regular font. This can be found on a few sites and is a Free font.
It looks like the M$ font Impact. You can change the font in the script.

Imagemagick

## Usage

./caption.sh \<input image> "Top text" "Bottom text" \<output image>
